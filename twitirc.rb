#!/usr/bin/ruby

require 'rubygems'
require 'net/yail/IRCBot'
require 'date'
require 'getopt/long'
gem('twitter4r', '>=0.2.1')
require 'twitter'

class TwitIrc < IRCBot
  BOTNAME = 'TwitIrc'
  BOTVERSION = 'v0.1.0'

  public
  #Starts a new instance
  #
  #Options:
  # * <tt>:irc_network</tt>: IP/name of server
  # * <tt>:port</tt>: ...
  # * <tt>:loud</tt>: Overly-verbose logging
  # * <tt>:silent</tt>: Very little logging
  # * <tt>:master</tt>: User who can order quits
  # * <tt>:passwords</tt>: Hash of channel=>pass for joining channels
  # * <tt>:twitter_passwords</tt>: Hash of Twitter username=>password
  # * <tt>:channels</tt>: Hash of IRC channel=>Twitter account
  def initialize(options = {})
    @master       = options.delete(:master)
    @passwords    = options[:passwords] || {}
    twitter_passwords = options.delete(:twitter_passwords)
    @channels     = options.delete(:channels)
    options[:channels] = @channels.keys
    @last_dm_id   = options.delete(:last_dm_id) || 0
    @last_searched_id = options.delete(:last_searched_id) || 0

    @last_poll_time = Time.new() - 1000 #Make it old enough to ensure that we poll the first time around

    options[:username] = BOTNAME
    options[:realname] = BOTNAME
    options[:nicknames] = ['twitirc', 'twitirc_', 'irctwit', 'irctwit_']

    #Create Twitter clients
    @twitter_clients = {}
    @channels.each do |channel, twitter_username|
      password = twitter_passwords[twitter_username]
      @twitter_clients[channel.downcase] = Twitter::Client.new(:login => twitter_username, :password => password)
    end

    #Set up IRCBot, our loving parent, and begin
    super(options)
    self.connect_socket
    self.start_listening
  end

  #Add hooks on startup (base class's start method calls add_custom_handlers)
  def add_custom_handlers
    #Set up hooks
    @irc.prepend_handler(:incoming_msg,             self.method(:_in_msg))
    @irc.prepend_handler(:incoming_act,             self.method(:_in_act))
    @irc.prepend_handler(:incoming_invite,          self.method(:_in_invited))
    @irc.prepend_handler(:incoming_kick,            self.method(:_in_kick))
    @irc.prepend_handler(:irc_loop,                 self.method(:_loop))

    @irc.prepend_handler(:outgoing_join,            self.method(:_out_join))
  end

  private
  #Incoming message handler
  def _in_msg(fullactor, user, channel, text)
    #check if this is a /msg command, or normal channel talk
    if channel =~ /#{bot_name}/
      incoming_private_message(user, text)
    else
      incoming_channel_message(user, channel, text)
    end
  end

  def _in_act(fullactor, user, channel, text)
    # check if this is a /msg command, or normal channel talk
    return if (channel =~ /#{bot_name}/)
  end

  # Gives the user very simplistic information.  TODO: Add a command for
  # accessing and searching logs.
  def incoming_private_message(user, text)
    msg(user, "I do not tweet private messages. Talk to me in public, please.")
  end

  def incoming_channel_message(user, channel, text)
    #check for special stuff before keywords
    #Botmaster is allowed to do special ordering
    if @master == user
      if (text == "#{bot_name}: QUIT")
        self.irc.quit("Ordered by my master")
        sleep 1
        exit
      end
    end

    channel.downcase!
    case text
      when /^\s*#{bot_name}(:|)\s*uptime\s*$/i
        msg(channel, get_uptime_string)
      when /botcheck/i
        msg(channel, "#{BOTNAME} #{BOTVERSION}")
      when /#{bot_name}: follow @?([a-zA-Z0-0_-]+)/
        if @twitter_clients.key?(channel)
          begin
            @twitter_clients[channel].friend(:add, $1)
            puts "Following #{$1}"
            msg(user, "Now following #{$1}")
          rescue
            puts "Error '#{$!}' following #{$1}"
            msg(channel, "#{user}: error '#{$!}' following #{$1}")
          end
        end        
      when /^.*#{bot_name}: [dD][mM] @?([a-zA-Z0-0_-]+)\s+(.+)\s*$/
        if @twitter_clients.key?(channel)
          begin
            @twitter_clients[channel].message(:post, "#{user}: #{$2}", $1)
            puts "DMing '#{$2}' to #{$1}"
            msg(user, "DMed '#{$2}' to #{$1}")
          rescue
            puts "Error '#{$!}' DMing '#{$2}' to #{$1}"
            msg(channel, "#{user}: error '#{$!}' DMing #{$1}")
          end
        end        
      when /^.*#{bot_name}:\s*(.+)\s*$/i
        puts "Got message: '#{$1}' from #{user} in #{channel}"
        if @twitter_clients.key?(channel)
          begin
            @twitter_clients[channel].status(:post, "#{user}: #{$1}")
            msg(user, "Tweet tweet!")
          rescue
            puts "Error '#{$!}' tweeting #{$1}"
            msg(channel, "#{user}: error '#{$!}' tweeting")
          end
        end
    end
  end

  #Invited to a channel - simply auto-join for now.
  #Maybe allow only @master one day, or array of authorized users.
  def _in_invited(fullactor, actor, target)
    join target
  end

  #If bot is kicked, he must rejoin!
  def _in_kick(fullactor, actor, target, object, text)
    if object == bot_name
      #Rejoin almost immediately
      join target
    end

    return true
  end

  def _loop()
    if Time.new() - @last_poll_time > 120 #Only poll once every two minutes, to stay under Twitter's rate limet (150 requests/hour)
      @last_poll_time = Time.new()
      new_last_dm_id = @last_dm_id
      new_last_searched_id = @last_searched_id

      @twitter_clients.each do |channel, twitter_client|
        begin
          #Poll for DMs
          messages = twitter_client.messages(:received)
          #Loop through messages (oldest first)
          messages.reverse_each do |message|
            if message.id > @last_dm_id
              puts "#{message.sender.screen_name} DMs: #{message.text}"
              msg(channel, "#{message.sender.screen_name} DMs: #{message.text}")
              new_last_dm_id = message.id if message.id > new_last_dm_id
            end
          end

          #Poll for statuses mentioning us
          statuses = twitter_client.search(:q => "@#{twitter_client.my(:info).screen_name}")
          statuses.reverse_each do |status|
            if status.id > @last_searched_id
              puts "#{status.from_user}: #{status.text}"
              msg(channel, "#{status.from_user}: #{status.text}")
              new_last_searched_id = status.id if status.id > new_last_searched_id
            end
          end
        rescue
          puts "Twitter fail: #{$!}"
        end
      end

      @last_dm_id = new_last_dm_id
      @last_searched_id = new_last_searched_id
      puts "last_dm_id now #{@last_dm_id}, last_searched_id #{@last_searched_id}"
    end
  end

  #We're trying to join a channel - use key if we have one
  def _out_join(target, pass)
    key = @passwords[target]
    pass.replace(key) unless key.to_s.empty?
  end
end


opt = Getopt::Long.getopts(
  ['--silent',      '-s', Getopt::BOOLEAN],
  ['--loud',        '-l', Getopt::BOOLEAN],
  ['--network',     '-n', Getopt::OPTIONAL],
  ['--master',      '-m', Getopt::OPTIONAL],
  ['--yaml',        '-y', Getopt::OPTIONAL]
)

opt['yaml'] ||= File.dirname(__FILE__) + '/default.yml'
if File.exists?(opt['yaml'])
  options = File.open(opt['yaml']) {|f| YAML::load(f)}
else
  options = {}
end

for key in %w{silent loud network output-dir master}
  options[key] ||= opt[key]
end

@bot = TwitIrc.new(
  :silent       => options['silent'],
  :loud         => options['loud'],
  :irc_network  => options['network'],
  :master       => options['master'],
  :passwords    => options['passwords'],
  :twitter_passwords => options['twitter_passwords'],
  :channels     => options['channels'],
  :last_dm_id   => options['last_dm_id'],
  :last_searched_id   => options['last_searched_id']
)
@bot.irc_loop
